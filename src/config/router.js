import { createWebHistory, createRouter } from "vue-router";
import Home from '../views/Home.vue'
import Pokemon from '../views/Pokemon.vue'
import Team from '../views/Team.vue'

const routes = [
    {
        path: '/',
        name: 'home',
        component: Home
    },
    {
        path: '/pokemon/:id',
        name: 'pokemon',
        component: Pokemon
    },
    {
        path: '/mon-equipe',
        name: 'team',
        component: Team
    },
]

const router = createRouter({
    history: createWebHistory(),
    routes
});


export default router